FROM composer:2 as cmp
FROM php:7.4-apache
COPY --from=cmp /usr/bin/composer /usr/bin/composer

ENV XDEBUG_MODE=coverage

RUN pecl install pecl/xdebug

RUN apt-get update && apt-get install \
        software-properties-common=0.96.20.2-2.1 \
        libzip-dev=1.7.3-1 \
        unzip=6.0-26 \
        git=1:2.30.2-1 \
        -yq --no-install-recommends  \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-install zip 
RUN docker-php-ext-enable xdebug zip



